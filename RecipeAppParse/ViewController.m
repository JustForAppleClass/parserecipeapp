//
//  ViewController.m
//  RecipeAppParse
//
//  Created by Matthew Griffin on 6/21/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "ViewController.h"
#import <parse/parse.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc]
                                  initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addRecipe:)];
    
    self.navigationItem.rightBarButtonItem = addButton;
    
    
   
   
   
   
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tableview = [[UITableView alloc]initWithFrame:CGRectMake(0, self.navigationController.navigationBar.frame.size.height + [UIApplication sharedApplication].statusBarFrame.size.height, self.view.frame.size.width, self.view.frame.size.height - self.navigationController.navigationBar.frame.size.height) style:UITableViewStylePlain];
   
    
    PFQuery *query = [PFQuery queryWithClassName:@"Recipe"];
    [query findObjectsInBackgroundWithBlock:^(NSArray* objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            
            self.recipe = objects;
            self.tableview.delegate = self;
            self.tableview.dataSource = self;
            [self.tableview reloadData];
            [self.view addSubview:self.tableview];
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.tableview removeFromSuperview];
}

-(void)addRecipe:(NSSet *)objects
{
    AddRecipeViewController* adder = [AddRecipeViewController new];
    [self.navigationController pushViewController:adder animated:YES];
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.recipe.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"tcell"];

    if(!cell)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"tcell"];
    }
    
    cell.textLabel.text = self.recipe[indexPath.row][@"RecipeTitle"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableview deselectRowAtIndexPath:indexPath animated:YES];
    RecipeViewController* recipe = [RecipeViewController new];
    recipe.recipe = self.recipe[indexPath.row];
    [self.navigationController pushViewController:recipe animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
