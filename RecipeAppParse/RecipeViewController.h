//
//  RecipeViewController.h
//  RecipeAppParse
//
//  Created by Michelle Griffin on 6/21/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "ViewController.h"
#import <parse/parse.h>


@interface RecipeViewController : UIViewController

@property(strong, nonatomic)PFObject* recipe;
@property(nonatomic, strong)UILabel* lbl;
@property(nonatomic, strong)UITextView* ingredients;
@property(nonatomic, strong)UITextView* instructions;


@end
