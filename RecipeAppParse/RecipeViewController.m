//
//  RecipeViewController.m
//  RecipeAppParse
//
//  Created by Michelle Griffin on 6/21/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "RecipeViewController.h"

@interface RecipeViewController ()

@end

@implementation RecipeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.ingredients = [UITextView new];
    self.lbl = [UILabel new];
    self.instructions = [UITextView new];
    
    
    self.view.backgroundColor = [UIColor grayColor];
    self.lbl.backgroundColor = [UIColor purpleColor];
    [self.lbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.lbl.textColor =[UIColor whiteColor];
    [self.ingredients setTranslatesAutoresizingMaskIntoConstraints: NO];
    [self.ingredients setEditable:NO];
    self.ingredients.backgroundColor = [UIColor purpleColor];
    self.ingredients.textColor = [UIColor whiteColor];
    
    
    [self.instructions setTranslatesAutoresizingMaskIntoConstraints: NO];
    self.instructions.backgroundColor = [UIColor orangeColor];
    self.instructions.textColor =[UIColor whiteColor];
    [self.instructions setEditable:NO];
    
    self.lbl.backgroundColor = [UIColor orangeColor];
    
    self.lbl.text = self.recipe[@"RecipeTitle"];
    self.instructions.text = self.recipe[@"Instructions"];
    self.ingredients.text = self.recipe[@"Ingredients"];
    
    [self.view addSubview:self.lbl];
    [self.view addSubview:self.ingredients];
    [self.view addSubview:self.instructions];
    
    NSDictionary *metrics = @{};
    NSDictionary *views = @{
                            @"one":self.lbl,
                            @"two":self.ingredients,
                            @"three":self.instructions,
                            };
    [self.view addSubview:self.lbl];
    [self.view addSubview:self.instructions];
    [self.view addSubview:self.ingredients];
    
    NSArray* constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-==80-[one(<=30)]-[two(three)]-[three]-==10-|" options:NSLayoutFormatAlignAllRight |NSLayoutFormatAlignAllLeft metrics:metrics views:views];
    [self.view addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[one]-|" options:0 metrics:metrics views:views];
    [self.view addConstraints:constraints];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
