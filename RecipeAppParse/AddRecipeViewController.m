//
//  AddRecipeViewController.m
//  RecipeAppParse
//
//  Created by Matthew Griffin on 6/21/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import "AddRecipeViewController.h"

@interface AddRecipeViewController ()

@end

@implementation AddRecipeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.ingredients = [UITextView new];
    self.lbl = [UITextField new];
    self.instructions = [UITextView new];
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc]
                                  initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addbutton:)];
    
    self.navigationItem.rightBarButtonItem = addButton;
    
    self.view.backgroundColor = [UIColor grayColor];
    self.lbl.backgroundColor = [UIColor purpleColor];
    [self.lbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    self.lbl.textColor =[UIColor whiteColor];
    [self.ingredients setTranslatesAutoresizingMaskIntoConstraints: NO];
    [self.ingredients setEditable:YES];
    self.ingredients.backgroundColor = [UIColor purpleColor];
    self.ingredients.textColor = [UIColor whiteColor];
    
    
    [self.instructions setTranslatesAutoresizingMaskIntoConstraints: NO];
    self.instructions.backgroundColor = [UIColor orangeColor];
    self.instructions.textColor =[UIColor whiteColor];
    [self.instructions setEditable:YES];
    
    self.lbl.backgroundColor = [UIColor orangeColor];
    [self.view addSubview:self.lbl];
    [self.view addSubview:self.ingredients];
    [self.view addSubview:self.instructions];
    
    NSDictionary *metrics = @{};
    NSDictionary *views = @{
                            @"one":self.lbl,
                            @"two":self.ingredients,
                            @"three":self.instructions,
                            
                            };
    [self.view addSubview:self.lbl];
    [self.view addSubview:self.instructions];
    [self.view addSubview:self.ingredients];
    
    NSArray* constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-==80-[one(<=30)]-[two(three)]-[three]-==10-|" options:NSLayoutFormatAlignAllRight |NSLayoutFormatAlignAllLeft metrics:metrics views:views];
    [self.view addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[one]-|" options:0 metrics:metrics views:views];
    [self.view addConstraints:constraints];
    
    
    
}



-(void)addbutton:(NSSet *)objects
{
 
    
    if([self.lbl hasText] && [self.ingredients hasText] && [self.instructions hasText])
    {
        
        
    NSMutableURLRequest *request =[[NSMutableURLRequest alloc]init];
    [request setURL:[NSURL URLWithString:@"https://api.parse.com/1/classes/Recipe"]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"vG5scCRpGkwTpUSQvhEykKsbdr5j70Mfii7Kqbs8" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request setValue:@"H83fEyGqr8uSegtraWEkmbo18Q1r8NK9jhAWAQRv" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    NSError* error;
    NSDictionary *dictionary = @{@"RecipeTitle": self.lbl.text,      @"Ingredients": self.ingredients.text, @"Instructions":self.instructions.text};
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    
    [request setHTTPBody:jsonData];
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request returningResponse:&requestResponse error:nil];
    
    NSString *requestReply = [[NSString alloc] initWithBytes:[requestHandler bytes] length:[requestHandler length] encoding:NSASCIIStringEncoding];
    NSLog(@"requestReply: %@", requestReply);

        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
