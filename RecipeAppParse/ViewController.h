//
//  ViewController.h
//  RecipeAppParse
//
//  Created by Matthew Griffin on 6/21/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecipeViewController.h"
#import "AddRecipeViewController.h"

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>


@property(strong, nonatomic) NSArray* recipe;
@property(strong, nonatomic) UITableView* tableview;



@end

