//
//  AddRecipeViewController.h
//  RecipeAppParse
//
//  Created by Matthew Griffin on 6/21/15.
//  Copyright (c) 2015 Matthew Griffin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <parse/parse.h>

@interface AddRecipeViewController : UIViewController
@property(nonatomic, strong)UITextField* lbl;
@property(nonatomic, strong)UITextView* ingredients;
@property(nonatomic, strong)UITextView* instructions;

@end
